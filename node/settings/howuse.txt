УСТАНОВКА:
1. устанавливаем node.js через nvm


СОЗДАНИЕ И НАСТРОЙКА ПРОЕКТА
1. создать папку
2. инициализировать package.json npm init -y
3. ставим Express.js через npm install express
4. для комфортной разработки, ставим Nodemon через npm install nodemon
5. настраимваем Nodemon в package.json
	"scripts": {
	    "serve": "nodemon app.js",
	    "start": "node app.js"
	},
