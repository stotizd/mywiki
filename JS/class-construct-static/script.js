const posts = document.querySelector('.posts');

class Post {
  constructor(head, disc) {
    this.head = head;
    this.disc = disc;
  }
  static async getPost() {
    const getItems = await fetch('https://jsonplaceholder.typicode.com/posts'),
    items = await getItems.json();
    for(let i = 0; i <= 5; i++) {
      const get = new Post(items[i].title, items[i].body).render()
    }
  };
  render() {
    posts.innerHTML += `
         <div class="post">
           <h3>${this.head}</h3>
           <span>${this.disc}</span>
         </div>
    `;
  };

}
Post.getPost()


class User {
    constructor(email, name) {
        this.email = email;
        this.name = name;
    }
    login() {
        console.log(this.email, 'logged in');
        return this
    }
}

class Admin extends User {
    deleteUser(user) {
        users = users.filter((usr) => {
            return usr.email != user.email
        })
    }
}

let userOne = new User('stotizd@gmail.com', 'Azamat');
let userTwo = new User('shoun@gmail.com', 'Paul');
let admin = new Admin('riko@gmail.com', 'Riko')
let users = [userOne, userTwo, admin];

admin.deleteUser(userOne)
console.log(users);
