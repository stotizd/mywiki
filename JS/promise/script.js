/* 
Promise имеет три состояния:
1) Pending - ожидание, исходная опперация не заврешена
2) Fulfilled - операция удачно завершена
3) Rejected - операция не удалась, ошибка
*/
console.log('Loading');
//Ассинхронность на setTimeout()
console.log("Ассинхроность на setTimeout()");
const obj = {
    name: 'Shlepa'
};
setTimeout(() => {
    console.log('Loading 100%');
    setTimeout(() => {        
        console.log('Data:', obj);
    }, 500);
}, 500);




//Promise
const ep = new Promise((resolve, reject) => {
    setTimeout(() => {
        const obj2 = {
            name: 'Shlepa',
            no: '2',
        }
        console.log("Ассинхроность на Promise");
        console.log('Data sending');
        resolve(obj2);
    }, 1500);
});
ep.then((obj2) => {
    return new Promise((resolve, reject) => {
        console.log('Data modified');
        obj2.modified = true;
    resolve(obj2);
    })  
})
//.then- выполнить след опперацию, после предыдущей
.then((obj2) => {
    console.log('Data:', obj2);
})
//.finally- выполнить когда все методы завершены
.finally(() => {
    console.log('Final');
})
//.catch- выполнить при ошибке
.catch(() => {
    console.log('Error');
})
