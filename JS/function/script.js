//Обычная функция, с свойствами a,b которые возращаются с помощью return
function primer(a, b) {
    return a + b   
}
console.log(primer(4, 5));



//Стрелочная функция
const ab = (a, b) => {
    return a + b
}
//если у стрелочной функций один аргумент, можно убрать скобки 
console.log(ab(5, 5));



//Когда использовать, а когда не надо стрел функц.
//Стрелочная функция принимает внешний this, т.е ее нельзя использовать так:
const div = document.querySelector('div');
number.addEventListiner('click', () => {
    this.style.color = 'black';
})
//Правильное использование
number.addEventListiner('click', function() {
    this.style.color = 'black';
    setTimeout(() => {
        this.style.weight = 'bold';
    })
})



/*Callback
такие функций нужны для изменения методов, без изменения самих функций
*/
function YourName3(callback) {
    let a = [3,5,45,34,5];
    callback(a);
}

function callminus(a) {
    console.log(a.join('-'));
    
}
function callplus(a) {
    console.log(a.join('+'));
}
YourName3(callplus);