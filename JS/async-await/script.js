// async
async function f1() {
    let a = await setTimeout(() => {
        console.log(1);
        
    }, 1000);
    return a
}

async function f2() {
    console.log(2);
}

async function f3() {
    console.log(3);
}

async function go() {
    let a = await f1();
    let b = await f2();
    let c = await f3();
}

go();