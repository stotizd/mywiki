const displayWidth = document.querySelector('.width'),
    displayHeight = document.querySelector('.height');

setInterval(() => {
    displayHeight.textContent =`Высота: ${document.documentElement.clientHeight}px`;
    displayWidth.textContent =`Ширина: ${document.documentElement.clientWidth}px`;
},300)


const scrollTop = document.querySelector('.scroll');

setInterval(() => {
    scrollTop.textContent = `${document.documentElement.scrollTop}px`;
},200)



const divWidth = document.querySelector('.width2'),
    divHeight = document.querySelector('.height2'),
    div = document.querySelector('.client');
    

setInterval(() => {
    divWidth.textContent =`Ширина: ${getComputedStyle(div).width}`;
    divHeight.textContent =`Высота: ${getComputedStyle(div).height}`;
},400)




let drag = document.querySelector('.drag'),
    dragX;

drag.addEventListener('dragstart', (e) => {
    dragX = e.screenX
});

drag.addEventListener('dragend', (e) => {
    console.log(e.screenX);
    let diff = dragX - e.screenX;
    div.style.width = `${div.clientWidth - diff * 2}px`;
    console.log(diff);
});