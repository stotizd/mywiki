{
  let arr = ['Apple', 'Pen', 'Pineapple'];
  arr.splice(0, 1, 'Samsung');
  arr.splice(1, 0, 'Samsa');
  console.log(arr);
}
{
  let arr = ['Apple', 'Pen', 'Pineapple', 'Fruit'];
  console.log(arr.slice(1));
  console.log(arr.slice(1, 3));
}
{
  let arr = ['Apple', 'Pen', 'Pineapple'];
  console.log(arr.shift());
}
{
  let arr = ['Apple', 'Pen', 'Pineapple'];
  arr.unshift('Reagle');
  console.log(arr);
}
{
  let arr = ['Apple', 'Pen', 'Pineapple'],
      arr2 = ['1', '2', '3'];
  let newArr = arr.concat(arr2);
  console.log(newArr);
}
