let num = 5.85;
console.log(Math.round(num));
console.log(Math.round(num * 10) / 10);

let str = 'test';
let sum = 5 / str;
console.log(sum);
console.log(sum === NaN);
console.log(isNaN(sum));

console.log(isFinite('45')); //true
console.log(isFinite('45rek')); //false
console.log(isFinite('45+2-47')); //true
console.log(isFinite(45)); //true

{

  let num = '222rek';
  console.log(+num, Number(num));
  console.log(parseInt(num));

  let num2 = '222.5rek';
  console.log(parseFloat(num2));

}
