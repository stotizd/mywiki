let massive = [1,2,3,4,5,6,7,8,9,10];
 
const map = massive.map(item => item + 1),
  filter = massive.filter(item => item >= 5),
  reduce = massive.reduce((num, num2) => num + num2),
  find = massive.find(item => item == 2),
  indexOf = massive.findIndex(item => item == 2),
  some = massive.some(item => item <= 10),
  every = massive.every(item => item <= 10);


console.log(map);
console.log(filter);
console.log(reduce);
console.log(find);
console.log(indexOf);
console.log(some);
console.log(every);