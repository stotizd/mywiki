const example1 = document.querySelectorAll('.example1 button');
const wrapper = document.querySelectorAll('.wrapper div');

for(let i = 0; i < example1.length; i++){
    for(let j = 0; j < wrapper.length; j++){
        example1[i].addEventListener('click', () => {
            example1[j].style.color = 'black';
            example1[i].style.color = example1[i].id;
            wrapper[j].style.display = 'none';
            wrapper[i].style.display = 'block';
        })
    };
};
