УСТАНОВКА WEBPACK:

1) Создать package.json
npm init

2) Установка webpack
npm i -D webpack webpack-cli

3) Добавить скрипт для сборки в package.json:
"build": "./node_modules/.bin/webpack"

4) Создать файл:
touch webpack.config.js

5) Добавить модуль в webpack.config 
const path = require('path');

module.exports = {
	mode: 'development',
	entry: './src/index.js',
	output: {
	  path: path.resolve(__dirname, 'dist'),
	  filename: 'bundle.js',
	},
};

5) Запустить webpack
npm run build



УСТАНОВКА МОДУЛЕЙ:

(babel в примере)
1) Установка:
npm install -D babel-loader @babel/core @babel/preset-env webpack

2) Вставить модуль в webpack.config
module: {
  rules: [
    {
      test: /\.m?js$/,
      exclude: /(node_modules|bower_components)/,
      use: {
        loader: 'babel-loader',
        options: {
          presets: ['@babel/preset-env']
        }
      }
    }
  ]
}

(css-loader в примере. Обязательно в паре с style-loader)

1) Установка:
style-loader: npm install --save-dev style-loader
css-loader: npm install --save-dev css-loader

2)
